package com.aibles.micorservice.saga.order.service.kafka;

import com.aibles.micorservice.saga.order.model.Orders;
import com.aibles.micorservice.saga.order.repository.OrderRepository;
import com.leonrad.saga.dto.orchestration.OrderOrchestratorResponseDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@RequiredArgsConstructor
@Service
public class KafkaSubcriberIml implements KafkaSubscriber{

    private final OrderRepository orderRepository;

    @Override
    public void OrderOrchestratorListener(OrderOrchestratorResponseDTO orderOrchestratorResponseDTO) {
        Orders orders = orderRepository.findById(orderOrchestratorResponseDTO.getOrderId()).get();
        updateOrderResult(orderOrchestratorResponseDTO, orders);
    }

    private void updateOrderResult(OrderOrchestratorResponseDTO orderOrchestratorResponseDTO, Orders orders){
        orders.setOrderStatus(orderOrchestratorResponseDTO.getOrderStatus());
        orderRepository.save(orders);
    }
}
