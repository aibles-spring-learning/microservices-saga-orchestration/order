package com.aibles.micorservice.saga.order.service.kafka;

import com.leonrad.saga.dto.orchestration.OrderOrchestratiorRequestDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;

import java.util.concurrent.ExecutionException;

@Service
@RequiredArgsConstructor
@Slf4j
public class KafkaPublisherIml implements KafkaPublisher{

    private final KafkaTemplate<String, OrderOrchestratiorRequestDTO> kafkaTemplate;

    @Value("${kafka.order.topic.publisher}")
    private String orderPublisherTopic;

    @Override
    public void sendMessage(OrderOrchestratiorRequestDTO orderOrchestratiorRequestDTO) {
        log.info("topic: {}", orderPublisherTopic);
        try {
            SendResult result = kafkaTemplate.send(orderPublisherTopic, "test" ,orderOrchestratiorRequestDTO).get();
            log.info("send message to order publisher completed: {}", result.toString());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
}
