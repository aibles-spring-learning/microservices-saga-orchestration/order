package com.aibles.micorservice.saga.order.service;

import com.aibles.micorservice.saga.order.model.Orders;
import com.aibles.micorservice.saga.order.repository.OrderRepository;
import com.aibles.micorservice.saga.order.service.kafka.KafkaPublisher;
import com.leonrad.saga.dto.orchestration.OrderOrchestratiorRequestDTO;
import com.leonrad.saga.dto.order.OrderRequestDTO;
import com.leonrad.saga.dto.order.OrderResponseDTO;
import com.leonrad.saga.dto.order.OrderStatus;
import com.leonrad.saga.dto.utils.EntityMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
@Slf4j
public class OrderServiceIml implements OrderService{

    private final OrderRepository orderRepository;
    private final KafkaPublisher kafkaPublisher;

    @Override
    public void createOrder(OrderRequestDTO orderRequestDTO) {
        Orders orderTarget = EntityMapper.map(orderRequestDTO, Orders.class);
        orderTarget.setOrderStatus(OrderStatus.CREATED);
        Orders order = orderRepository.save(orderTarget);
        final OrderOrchestratiorRequestDTO orderOrchestratiorRequestDTO = buildOrderOrchestratorRequestDTO(order);
        kafkaPublisher.sendMessage(orderOrchestratiorRequestDTO);
    }

    @Override
    public List<OrderResponseDTO> listOrder(int userId) {
        final List<Orders> orders = orderRepository.findByUserId(userId);
        return EntityMapper.mapList(orders, OrderResponseDTO.class);
    }

    private OrderOrchestratiorRequestDTO buildOrderOrchestratorRequestDTO (Orders order){
        return OrderOrchestratiorRequestDTO.builder()
                .orderId(order.getId())
                .userId(order.getUserId())
                .productId(order.getProductId())
                .build();
    }
}
