package com.aibles.micorservice.saga.order.service;

import com.leonrad.saga.dto.order.OrderRequestDTO;
import com.leonrad.saga.dto.order.OrderResponseDTO;

import java.util.List;

public interface OrderService {
    void createOrder(OrderRequestDTO orderRequestDTO);
    List<OrderResponseDTO> listOrder(int userId);
}
