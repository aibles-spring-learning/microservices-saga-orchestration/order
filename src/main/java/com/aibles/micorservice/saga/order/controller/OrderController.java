package com.aibles.micorservice.saga.order.controller;

import com.aibles.micorservice.saga.order.service.OrderService;
import com.leonrad.saga.dto.order.OrderRequestDTO;
import com.leonrad.saga.dto.order.OrderResponseDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/orders")
@RequiredArgsConstructor
public class OrderController {

    private final OrderService orderService;

    @PostMapping
    public ResponseEntity<OrderResponseDTO> createOrder(@RequestBody OrderRequestDTO orderRequestDTO){
        orderService.createOrder(orderRequestDTO);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
