package com.aibles.micorservice.saga.order.service.kafka;

import com.leonrad.saga.dto.orchestration.OrderOrchestratiorRequestDTO;

public interface KafkaPublisher {
    void sendMessage(OrderOrchestratiorRequestDTO orderOrchestratiorRequestDTO);
}
