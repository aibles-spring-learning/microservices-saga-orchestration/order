package com.aibles.micorservice.saga.order.service.kafka;

import com.leonrad.saga.dto.orchestration.OrderOrchestratorResponseDTO;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.KafkaListener;

@Configuration
public interface KafkaSubscriber {

    @KafkaListener(
            topics = "${kafka.order.topic.consumer}",
            containerFactory = "${kafka.order.consumer.container.factory}"
    )
    void OrderOrchestratorListener(OrderOrchestratorResponseDTO orderOrchestratorResponseDTO);

}
